# Animations

Animations is a project to learn about animations.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/animation-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Implicit animations
- Explicit animations
- Binding animations
- Multiple animations
- Gesture animations
- Transitions