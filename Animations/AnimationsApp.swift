//
//  AnimationsApp.swift
//  Animations
//
//  Created by Pascal Hintze on 02.11.2023.
//

import SwiftUI

@main
struct AnimationsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
